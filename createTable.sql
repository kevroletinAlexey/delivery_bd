CREATE TABLE country
(
	id_country SERIAL PRIMARY KEY,
	name varchar(50)
);

CREATE TABLE city
(
	id_city SERIAL PRIMARY KEY,
	name varchar(50) NOT NULL
);

CREATE TABLE address
(
    id_address SERIAL PRIMARY KEY,
    id_country integer REFERENCES country (id_country) ON DELETE CASCADE,
    id_city integer REFERENCES city (id_city) ON DELETE CASCADE,
    street varchar(50) NOT NULL,
    house varchar(40) NOT NULL,
    flat varchar(20) NOT NULL,
    postal_code varchar(20) NOT NULL
);

CREATE TABLE courier
(
    id_courier SERIAL PRIMARY KEY,
    last_name varchar(128) NOT NULL,
    first_name varchar(64) NOT NULL,
    birth_date date NOT NULL,
    phone varchar(50) NOT NULL UNIQUE,
    mail varchar(256) NOT NULL UNIQUE,
    bank_card varchar(256) NOT NULL,
    date_employment date NOT NULL
);

CREATE TABLE address_courier
(
    id_address integer REFERENCES address (id_address) ON DELETE CASCADE,
    id_courier integer REFERENCES courier (id_courier) ON DELETE CASCADE,
    PRIMARY KEY (id_address, id_courier)
);

CREATE TABLE legal_entity
(
    id_legal_entity SERIAL PRIMARY KEY,
    name varchar(256) NOT NULL,
    phone varchar(50) NOT NULL UNIQUE,
    inn varchar(50) NOT NULL,
    kpp varchar(50) NOT NULL,
    okpo varchar(50) NOT NULL,
    ogrn varchar(50) NOT NULL,
    bik varchar(50) NOT NULL
);

CREATE TABLE branch
(
    id_branch SERIAL PRIMARY KEY,
    id_legal_entity integer REFERENCES legal_entity (id_legal_entity) ON DELETE CASCADE,
    id_address integer REFERENCES address (id_address) ON DELETE CASCADE,
    phone varchar(50) NOT NULL UNIQUE,
    opening_time time,
    closing_time time
);

CREATE TABLE customer
(
    id_customer SERIAL PRIMARY KEY,
    last_name varchar(128),
    first_name varchar(64),
    birth_date date,
    phone varchar(50) NOT NULL UNIQUE,
    mail varchar(256) NOT NULL UNIQUE,
    bank_card varchar(256) NOT NULL,
    registration_date date NOT NULL DEFAULT current_date,
    id_address integer REFERENCES address (id_address) ON DELETE CASCADE
);

CREATE TABLE category
(
    id_category SERIAL PRIMARY KEY,
    name varchar(128)
);

CREATE TABLE brand
(
    id_brand SERIAL PRIMARY KEY,
    name varchar(128),
    id_country integer REFERENCES country (id_country) ON DELETE RESTRICT
);

CREATE TABLE product
(
    id_product SERIAL PRIMARY KEY,
    name varchar(256) NOT NULL,
    description text,
    id_brand integer REFERENCES brand (id_brand) ON DELETE CASCADE,
    id_category integer REFERENCES category (id_category) ON DELETE CASCADE
);

CREATE TABLE photo_product
(
    id_photo_product SERIAL PRIMARY KEY,
    link varchar(256) NOT NULL,
    id_product integer REFERENCES product (id_product) ON DELETE CASCADE
);

CREATE TABLE property
(
    id_property SERIAL PRIMARY KEY,
    name varchar(256) NOT NULL
);

CREATE TABLE product_property
(
    id_product integer REFERENCES product (id_product) ON DELETE CASCADE,
    id_property integer REFERENCES property (id_property) ON DELETE CASCADE,
    value varchar(512),
    PRIMARY KEY(id_product, id_property)
);

CREATE TABLE legal_entity_product
(
    id_legal_entity_product SERIAL PRIMARY KEY,
    id_legal_entity integer REFERENCES legal_entity (id_legal_entity) ON DELETE CASCADE,
    id_product integer REFERENCES product (id_product) ON DELETE CASCADE,
    price money NOT NULL,
    count integer NOT NULL CHECK ( count >= 0 ),
    discount integer CHECK ( discount >= 0 )
);

CREATE TABLE client_order_status
(
    id_client_order_status SERIAL PRIMARY KEY,
    value varchar(256) NOT NULL
);

CREATE TABLE client_order
(
    id_client_order SERIAL PRIMARY KEY,
    id_customer integer REFERENCES customer (id_customer) ON DELETE RESTRICT,
    id_address integer REFERENCES address (id_address) ON DELETE RESTRICT,
    date_creation timestamptz NOT NULL DEFAULT current_timestamp,
    id_courier integer REFERENCES courier (id_courier) ON DELETE RESTRICT,
    id_branch integer REFERENCES branch (id_branch) ON DELETE RESTRICT
);

CREATE TABLE client_order_branch_product
(
    id_legal_entity_product integer REFERENCES legal_entity_product (id_legal_entity_product) ON DELETE RESTRICT,
    id_client_order integer REFERENCES client_order (id_client_order) ON DELETE CASCADE,
    count integer NOT NULL DEFAULT 1 CHECK ( count > 0 )
);

CREATE TABLE client_order_status_update
(
    id_client_order_status_update SERIAL PRIMARY KEY,
    id_client_order integer REFERENCES client_order(id_client_order) ON DELETE CASCADE,
    id_client_order_status integer REFERENCES client_order_status(id_client_order_status) ON DELETE CASCADE,
    update_time timestamptz NOT NULL
);


