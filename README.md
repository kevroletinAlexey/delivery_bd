# Study project on the course "Databases" of MEPhI

## Логическая модель

![](img/Логическая_модель_бд-Page-1.drawio.svg)

## Физическая модель

![](img/Физическая_модель_бд-Page-1.drawio.svg)

## Описание запросов к полученной БД.

1. Определить продукты какого бренда были добавлены в наибольшее число заказов и доставлены.

```sql
WITH delivered_orders AS (
    SELECT id_client_order FROM client_order_status_update AS cosu
    JOIN client_order_status cos on cosu.id_client_order_status = cos.id_client_order_status
    WHERE cos.value = 'Доставлен'
), delivered_products AS (
    SELECT count(*) AS count, b.name FROM client_order
    JOIN client_order_branch_product cobp on client_order.id_client_order = cobp.id_client_order
    JOIN legal_entity_product lep on cobp.id_legal_entity_product = lep.id_legal_entity_product
    JOIN product p on lep.id_product = p.id_product
    JOIN brand b on b.id_brand = p.id_brand
    WHERE client_order.id_client_order IN (SELECT id_client_order FROM delivered_orders)
    GROUP BY b.id_brand
)
SELECT name, count FROM delivered_products
WHERE count = (SELECT max(count) FROM delivered_products);
```

2. Вывести по категориям общую цену, всех продуктов, добавленных в заказ для Москвы (с учетом скидки).

```sql
WITH products_sold_in_moscow AS (
    SELECT cobp.id_client_order, cobp.id_legal_entity_product  FROM client_order_branch_product AS cobp
    JOIN client_order co on cobp.id_client_order = co.id_client_order
    JOIN address a on a.id_address = co.id_address
    JOIN city c on c.id_city = a.id_city
    WHERE c.name = 'Москва'
), category_sales AS (
    SELECT p.id_category, sum((lep.price - (lep.discount / 100) * lep.price) * cobp.count) AS total_sales, c2.name
    FROM client_order_branch_product AS cobp
    JOIN legal_entity_product lep on cobp.id_legal_entity_product = lep.id_legal_entity_product
    JOIN product p on p.id_product = lep.id_product
    JOIN category c2 on c2.id_category = p.id_category
    WHERE cobp.id_client_order IN
          (SELECT products_sold_in_moscow.id_client_order FROM products_sold_in_moscow)
    GROUP BY p.id_category, c2.name
)
SELECT id_category, name, total_sales FROM category_sales;
```

3. Вывести все продукты, содержащие больше 5г белков.

```sql
WITH protein AS (
    SELECT product.name, pp.id_property, pp.value FROM product
    JOIN product_property pp on product.id_product = pp.id_product
    JOIN property p on p.id_property = pp.id_property
    WHERE p.name = 'Белки'
)
SELECT name, value FROM protein
WHERE value ~ '[0-9]$' and cast(value as float)  > 5;
```

4. Вывести id 5 курьеров, доставивших наименьшее число заказов, также вывести общую сумму заказов для каждого курьера.

```sql
WITH courier_order AS (
    SELECT count(id_client_order) AS count_order, client_order.id_courier
        FROM client_order
    GROUP BY client_order.id_courier
    ORDER BY count_order ASC
    LIMIT 5
), total_sum AS (
    SELECT sum(sum_order.total) as total_courier, id_courier FROM
        (SELECT cobp.id_client_order, sum(lep.price) AS total, co.id_courier
            FROM client_order_branch_product as cobp
        JOIN legal_entity_product lep on lep.id_legal_entity_product = cobp.id_legal_entity_product
        JOIN client_order co on cobp.id_client_order = co.id_client_order
        GROUP BY cobp.id_client_order, co.id_courier) AS sum_order
    WHERE sum_order.id_courier IN (SELECT id_courier FROM courier_order)
    GROUP BY sum_order.id_courier
)
SELECT total_sum.id_courier, total_sum.total_courier, courier_order.count_order  FROM total_sum
JOIN courier_order ON courier_order.id_courier = total_sum.id_courier;
```

5. Вывести адрес филиала, из которого доставлено товаров на наибольшую сумму, также вывести сумму.

```sql
WITH branch_order AS (
    SELECT sum(sum_order.total) as total_branch, id_branch FROM
        (SELECT cobp.id_client_order, sum(lep.price * cobp.count) AS total, co.id_branch
            FROM client_order_branch_product as cobp
        JOIN legal_entity_product lep on lep.id_legal_entity_product = cobp.id_legal_entity_product
        JOIN client_order co on cobp.id_client_order = co.id_client_order
        GROUP BY cobp.id_client_order, co.id_branch) AS sum_order
    GROUP BY sum_order.id_branch
)
SELECT c2.name, c.name, a.street, a.house, total_branch FROM branch
JOIN (SELECT id_branch, total_branch FROM branch_order
WHERE total_branch = (SELECT max(total_branch) FROM branch_order)) AS result
    ON result.id_branch = branch.id_branch
JOIN address a on a.id_address = branch.id_address
JOIN city c on c.id_city = a.id_city
JOIN country c2 on c2.id_country = a.id_country;
```